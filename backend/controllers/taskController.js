const Task = require('../models/Task');

// Ajouter une nouvelle tâche à une liste
exports.createTask = async (req, res) => {
  const { taskListId, description, details, dueDate } = req.body;
  try {
    const task = new Task({ taskListId, description, details, dueDate });
    await task.save();
    res.status(201).send(task);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Mettre à jour une tâche
exports.updateTask = async (req, res) => {
  const { id } = req.params;
  const { description, details, dueDate, completed } = req.body;
  try {
    const task = await Task.findByIdAndUpdate(id, { description, details, dueDate, completed }, { new: true });
    if (!task) return res.status(404).send({ message: 'Task not found' });
    res.status(200).send(task);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Supprimer une tâche
exports.deleteTask = async (req, res) => {
  const { id } = req.params;
  try {
    const task = await Task.findByIdAndDelete(id);
    if (!task) return res.status(404).send({ message: 'Task not found' });
    res.status(200).send({ message: 'Task deleted' });
  } catch (error) {
    res.status(500).send(error);
  }
};

// Obtenir toutes les tâches d'une liste spécifique
exports.getTasksByList = async (req, res) => {
  const { taskListId } = req.params;
  try {
    const tasks = await Task.find({ taskListId });
    res.status(200).send(tasks);
  } catch (error) {
    res.status(500).send(error);
  }
};

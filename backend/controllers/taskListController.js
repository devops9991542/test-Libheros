const TaskList = require('../models/taskList');

// Créer une nouvelle TaskList
exports.createTaskList = async (req, res) => {
  const { name, userId } = req.body;
  try {
    const taskList = new TaskList({ name, userId });
    await taskList.save();
    res.status(201).send(taskList);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Obtenir toutes les TaskLists d'un utilisateur
exports.getUserTaskLists = async (req, res) => {
  const { userId } = req.params;
  try {
    const taskLists = await TaskList.find({ userId }).populate('tasks');
    res.status(200).send(taskLists);
  } catch (error) {
    res.status(500).send(error);
  }
};

// Supprimer une TaskList et ses tâches associées
exports.deleteTaskList = async (req, res) => {
  const { id } = req.params;
  try {
    const taskList = await TaskList.findById(id);
    if (!taskList) return res.status(404).send({ message: 'TaskList not found' });
    await Task.deleteMany({ taskListId: id });
    await taskList.remove();
    res.status(200).send({ message: 'TaskList and all associated tasks deleted' });
  } catch (error) {
    res.status(500).send(error);
  }
};

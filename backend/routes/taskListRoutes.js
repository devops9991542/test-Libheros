const express = require('express');
const router = express.Router();
const taskListController = require('../controllers/taskListController');
const auth = require('../middleware/auth');

router.post('/', auth, taskListController.createTaskList);
router.get('/:userId', auth, taskListController.getUserTaskLists);
router.delete('/:id', auth, taskListController.deleteTaskList);

module.exports = router;

const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];  // Bearer <token>
    if (!token) {
      return res.status(401).send({ message: 'No token, authorization denied' });
    }

    const decoded = jwt.verify(token, "process.env.JWT_SECRET");
    req.user = decoded;  // Ajouter l'ID utilisateur à l'objet request
    next();
  } catch (error) {
    res.status(401).send({ message: 'Token is not valid' });
  }
};

module.exports = auth;

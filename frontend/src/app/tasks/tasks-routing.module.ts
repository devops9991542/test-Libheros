import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskListsComponent } from './task-lists/task-lists.component';
import { TaskDetailsComponent } from './task-details/task-details.component';

const routes: Routes = [
  { path: '', component: TaskListsComponent },
  { path: 'details/:id', component: TaskDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
export interface Task {
    id?: string;
    taskListId: string;
    description: string;
    details?: string;
    dueDate?: Date;
    completed: boolean;
  }  
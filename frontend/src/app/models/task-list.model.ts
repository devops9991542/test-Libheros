export interface TaskList {
    id?: string;
    name: string;
    userId: string;
    tasks: string[]; // Array of Task IDs
  }